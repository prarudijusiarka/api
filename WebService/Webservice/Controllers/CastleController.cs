﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Webservice.Models;
using Webservice.Models.BaseModels;

namespace Webservice.Controllers
{
    [Produces("application/json")]
    [Route("Castle")]
    [ApiController]
    public class CastleController : ControllerBase
    {
        private readonly CastleDto[] _castles = new[]
        {
            new CastleDto()
            {
                Name = "Kauno pilis",
                LocationPoint = new PointD(54.899078, 23.8839),
                Description =
                    "viena seniausių Lietuvos mūrinių pilių, stovinti Kaune, antrajame pagal dydį šalies mieste. Pirmą kartą rašytiniuose šaltiniuose paminėta 1361 m."
            },
            new CastleDto()
            {
                Description =
                    "stovi Raudondvaryje, dešiniajame Nevėžio krante. Architektūros ansamblis, kurį sudaro XVII a. 1-osios pusės renesanso pilies rūmai, šiaurinė ir pietinė oficinos, oranžerija, liokajaus namas, arklidės, ūkvedžio namelis, šiaurinis ir pietinis svirnai, ledainė ir parkas (užveistas XIX a.). Pilyje veikia kompozitoriaus J. Naujalio memorialinis muziejus (nuo 2002 m.). Parke organizuojami koncertai, festivaliai, parodos ir kiti renginiai. Dvaro mūrų spalva suteikė pavadinimą ir visai gyvenvietei. Nors rūmai vadinami pilimi ir turi pilies bruožų (bokštas, šaudymo angos), tačiau gynybinės reikšmės neturėjo, sienas būtų nesunkiai įveikusios to meto patrankos. Tai buvo reprezentaciniai rūmai.",
                LocationPoint = new PointD(54.9430937, 23.78304),
                Name = "Raudondvario dvaras"
            }
        };


        [HttpGet]
        public string Get()
        {
            var rng = new Random();

            var json = JsonConvert.SerializeObject(_castles[rng.Next(0, 1)]);
            return json;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /TODO
        ///     {
        ///         "Name" : "Alytaus Dvaras",
        ///         "LocationPoint" : {
        ///                "X" : 23.55,
        ///                "Y" : 55.16
        ///         },
        ///         "Description" : "čia geria milžinai"
        ///     }
        ///
        /// </remarks>
        /// <returns></returns>
        /// <response code="400">Todo</response>
        /// <response code="200">returns OK.</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Post()
        {
            return Ok();
        }
    }
}