﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Webservice.Services;
using Newtonsoft.Json;

namespace Webservice.Controllers
{
    [Produces("application/json")]
    [Route("LankytinaVieta")]
    [ApiController]
    public class LankytinaVietaController : ControllerBase
    {
        private readonly ILankytinaVietaService _lankytinaVietaService;

        public LankytinaVietaController(ILankytinaVietaService lankytiniVietaService)
        {
            _lankytinaVietaService = lankytiniVietaService;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get([FromRoute(Name = "id")] int id)
        {
            var vieta = await _lankytinaVietaService.GetVietaDto(id);
            if (vieta == null)
                return NotFound();

            return Ok(JsonConvert.SerializeObject(vieta));
        }

    }
}