﻿using System.ComponentModel.DataAnnotations;
using Webservice.Models.BaseModels;

namespace Webservice.Models
{
    public class CastleDto
    {
        [Required]
        public string Name;
        [Required]
        public PointD LocationPoint;
        public string Description;
    }
}
