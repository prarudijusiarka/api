﻿namespace Webservice.Models.BaseModels
{
    public class PointD
    {
        public double X { get; set; }
        public double Y { get; set; }

        public PointD(double x, double y)
        {
            X = x;
            Y = y;
        }

        public override bool Equals(object? obj)
        {
            if (obj == null)
                return false;

            if (obj is PointD other)
            {
                return X.Equals(other.X) && Y.Equals(other.Y);
            }

            return false;
        }
    }
}
