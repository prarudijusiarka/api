﻿namespace Webservice.Models
{
    public class LankytinaVietaDto
    {
        public string Pavadinimas { get; set; }
        public string Miestas { get; set; }
        public string Tipas { get; set; }
        public int Id { get; set; }
    }
}
