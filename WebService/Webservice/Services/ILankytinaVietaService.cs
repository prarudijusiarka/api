﻿using System.Threading.Tasks;
using Webservice.Models;

namespace Webservice.Services
{
    public interface ILankytinaVietaService
    {
        Task<LankytinaVietaDto> GetVietaDto(int id);
    }
}
