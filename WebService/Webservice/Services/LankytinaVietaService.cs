﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Webservice.Models;
using Webservice.Repository;

namespace Webservice.Services
{
    public class LankytinaVietaService : ILankytinaVietaService
    {
        private readonly ILankytinaVietaRepository _repository;
        private readonly ILogger<LankytinaVietaService> _logger;

        public LankytinaVietaService(ILankytinaVietaRepository repository, ILogger<LankytinaVietaService> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        public async Task<LankytinaVietaDto> GetVietaDto(int id)
        {
            try
            {
                return await _repository.GetVietaDto(id);
            }
            catch
            {
                _logger.LogError($"Could not retrieve {nameof(LankytinaVietaDto)} in LankytinaVietaService");
                return null;
            }
        }
    }
}
