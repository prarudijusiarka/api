﻿using System.Linq;
using System.Threading.Tasks;
using Api.Configuration;
using Dapper;
using Microsoft.Extensions.Options;
using Npgsql;
using Webservice.Models;

namespace Webservice.Repository
{
    public class LankytinaVietaRepository : ILankytinaVietaRepository
    {
        private readonly string _connectionString;
        
        public LankytinaVietaRepository(IOptions<ApiSettings> settings)
        {
            _connectionString = settings.Value.ConnectionStrings.PostgresConnectionString;
        }

        public async Task<LankytinaVietaDto> GetVietaDto(int id)
        {
            const string query = "SELECT * FROM public.lankytinavieta WHERE id = @id;";
            using (var conn = new NpgsqlConnection(_connectionString))
            {
                var parameters = new {id = id};

                return await conn.QuerySingleOrDefaultAsync<LankytinaVietaDto>(query, parameters);
            }
        }
    }
}
