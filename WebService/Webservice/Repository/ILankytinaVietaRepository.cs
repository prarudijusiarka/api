﻿using System.Threading.Tasks;
using Webservice.Models;

namespace Webservice.Repository
{
    public interface ILankytinaVietaRepository
    {
        Task<LankytinaVietaDto> GetVietaDto(int id);
    }
}
