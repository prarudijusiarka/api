﻿namespace Api.Configuration
{
    public class ApiSettings
    {
        public ConnectionStrings ConnectionStrings { get; set; }
        public Logging Logging { get; set; }
    }
}
