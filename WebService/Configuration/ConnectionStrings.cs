﻿namespace Api.Configuration
{
    public class ConnectionStrings
    {
        public string PostgresConnectionString { get; set; }
    }
}
