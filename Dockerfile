#copy and build
FROM mcr.microsoft.com/dotnet/core/sdk:3.1.102-alpine3.11 AS build-env
COPY WebService/ ./
RUN dotnet restore
RUN dotnet publish -c Release -o out --no-restore

#runtime
FROM mcr.microsoft.com/dotnet/core/sdk:3.1.102-alpine3.11
WORKDIR /app
EXPOSE 80
COPY --from=build-env /out .
ENTRYPOINT ["dotnet", "Webservice.dll"]
